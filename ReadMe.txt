This entry won first place in the "Single Developer" category of the 2011
rendition of the "Compsoc Java Competition!", a first year game development
competition run by the University of Edinburgh and its computer society Compsoc.
Prescribed language: Java.
Duration: 2 weeks.

Unfortunately, due to the competition being run before I started using version
control, I somehow managed to misplace the source code... but the jar files are
still around (run as desktop app or embed in iFrame).
